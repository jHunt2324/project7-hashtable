public class table {
    int size = 11;
    int hash = 11;

    Node bucket[] = new Node[size]; // does this not make an array that can hold 11 elements?

    // time complexity 0(1)
    void insert(String str) {
        Node newNode = new Node(str); //create a new node

        newNode.next = bucket[hash(str) % hash]; //Holds are key value

        bucket[hash(str) % hash] = newNode; //hold our new node here

    }

    int hash(String str) {
        int hashval = 0;
        int bucket = 53; //Hold a prime number here

        // loop through the string
        for(int i =0; i< str.length(); i++) {
            hashval = (hashval << 8) + str.charAt(i); // takes the ascii values and add it by 32
        }

        //divide the hash value by the prime number bucket
        hashval = hashval % bucket;

        return hashval;
    }

    void display() {
        Node temp;

        for(int i =0; i < size; i++) {
            int x = i % hash; //divide integer by array size
            temp = bucket[x]; // get the value from inside the loop

            while(temp != null) {
                System.out.print(temp.data + " "); //print out the temp data
                temp = temp.next;
            }
        }
        System.out.println();
    }

    //time complexity o(n)
    boolean find(String str) {
        Node temp;
        int x = hash(str) % hash; // call hash method % 11
        temp = bucket[x]; //start there in our array

        if(bucket[x] == null) {
            return false; // if nothing is there return null
        }

        //if the first element is there return that we have found it
        if(bucket[x].data.equals(str)) {
            System.out.println("Found at index " + x);
            return true;
        }
        //if we have found it but it is chained on
        else {
            while(temp != null) { //traverse through our chained nodes
                if(temp.data.equals(str)) { //when we find it return it
                    System.out.println("Found at index " + x);
                    return true;
                }
                temp = temp.next;
            }
        }
        System.out.println("Word was not found");
       return false;
    }
}
