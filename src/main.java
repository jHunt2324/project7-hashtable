//Jeffrey Hunt
//11-19-2021

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;

public class main {
    public static void main (String[] args) throws FileNotFoundException{
        //clc is on the radix sort
        table table = new table();
        Scanner fin = new Scanner(new File("text.txt")); //read in a file
        Scanner keyboard = new Scanner(System.in);


        //initial variables
        String str;
        String input;

        //put or values into the str
        while(fin.hasNext()) {
            str = fin.next();
            table.insert(str);
        }

        table.display();

        System.out.print("Enter a word: ");

        input = keyboard.next();
        table.find(input);


    }
}
